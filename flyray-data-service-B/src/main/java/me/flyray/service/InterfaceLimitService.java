package me.flyray.service;

import me.flyray.entity.InterfaceLimit;

public interface InterfaceLimitService {

	InterfaceLimit getEntityByPri(Integer id);
}
